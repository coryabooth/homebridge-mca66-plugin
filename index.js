const packagejson = require('./package.json');
const PLUGIN_NAME = packagejson.name;
const PLATFORM_NAME = packagejson.platformname;
const PLUGIN_VERSION = packagejson.version;
const NO_INPUT_ID = 999;
const NO_INPUT_NAME = 'UNKNOWN';
const net = require("net");

let Accessory, Characteristic, Service, Categories, UUID;

module.exports = (api) => {
	Accessory = api.platformAccessory;
	Characteristic = api.hap.Characteristic;
	Service = api.hap.Service;
	Categories = api.hap.Categories;
	UUID = api.hap.uuid;
	api.registerPlatform(PLUGIN_NAME, PLATFORM_NAME, HtdPlatform, true);
};

class HtdPlatform {
	constructor(log, config, api) {
		if (!config || !Array.isArray(config.zones)) {
			log('No configuration found for %s', PLUGIN_NAME);
			return;
		}
		this.log = log;
		this.config = config;
		this.api = api;
		this.zones = [];
		this.inputs = [];
		this.host = this.config.host
		this.port = this.config.port
		this.client = new net.Socket();
		this.updated_flag = false;
		this.config_zones_array = Object.values(this.config.zones[0]);

		this.api.on('didFinishLaunching', () => {
  		this.setupGateway(this.log);
			if (this.config_zones_array.length == 0) {
				this.log.warn('No zones found in config for %s', PLUGIN_NAME)
			} else {
				for (let i = 0, len = this.config_zones_array.length; i < len; i++) {
					this.log("Loading zone %s", i+1, this.config_zones_array[i]);
					let newZone = new HtdZone(this.log, this.config, this.api, this, i);
					this.zones.push(newZone);
				}
		  }
		});
	}
	
	eventData = (response) => {
    var response = this.decodeResponse(response);
    if(response) {
      this.updated_flag = true;
      var device = this.zones[(response['zone']) - 1];
			device.updateDeviceState(response['power'], response['input']);
			//this.debugOutput(response);
    } else {
      this.log.error('Unknown data repsonse string');
    }
  }
  
  getBinaryDigit = (decimalNumber, digit) => {
    var binary = decimalNumber.toString(2);
    return binary.substr(digit, 1);
  }
  
  twoDigitHex = (number) => {
    var hex = number.toString(16);
    return hex.padStart(2, "0");
  }
  
  getCommandNumberArray = (zone, commandCode, dataCode) => {
    return [2, 0, zone, commandCode, dataCode];
  }
  
  getHexCommand = (zone, commandCode, dataCode) => {
    var result = '';
    var numbers = this.getCommandNumberArray(zone, commandCode, dataCode);
    var checksum = 0;
    for(let i = 0, len = numbers.length; i < len; i++) {
			result = result + this.twoDigitHex(numbers[i]);
			checksum = checksum + numbers[i];
		}
    result = result + this.twoDigitHex(checksum);
    return result;
  }
  
  getCommandBytes = (zone, commandCode, dataCode) => {
    return this.getHexCommand(zone, commandCode, dataCode);
  }
  
  getZoneStates = (zone) => { 
    return this.getCommandBytes(zone, 6, 0);
  } 
  
  getSourceCommand = (zone, source) => {
    return this.getCommandBytes(zone, 4, 2 + source);
  }
  
  setPower = (zone, power) => {
    return this.getCommandBytes(zone, 4, power ? 32 : 33);
  }
  
  convertControllerVolumeToPercent = (volume) => {
    volume =  volume == 0 ? 256 : volume;
    volume = Math.round((volume - 196.0) / 60.0 * 100.0);
    return Math.max(0, Math.min(100, volume));
  }
  
  decodeResponse = (data) => {
  	var validResponse = false;
  	var data_array = new Buffer(data, 'ascii');
    var i = 0;
    while (i + 14 <= data_array.length) {
      var zoneData = data_array.slice(i, i+14);
      if(zoneData[0] == 2 && zoneData[1] == 0) {
        if(zoneData[3] == 5) {
          var zoneConfig = [];
          validResponse = true;
          zoneConfig['zone'] = zoneData[2];
          zoneConfig['power'] = this.getBinaryDigit(zoneData[4], 0) == 1 ? 1 : 0;
          zoneConfig['mute'] = this.getBinaryDigit(zoneData[4], 1) == 1 ? true : false;
          zoneConfig['mode'] = this.getBinaryDigit(zoneData[4], 2) == 1 ? 'attributes' : 'volume';
          zoneConfig['party'] = this.getBinaryDigit(zoneData[4], 3) == 1 ? true : false;
          zoneConfig['input'] = zoneData[8] + 1;
          zoneConfig['volume'] = this.convertControllerVolumeToPercent(zoneData[9]);
          zoneConfig['treble'] = zoneData[10];
          zoneConfig['bass'] = zoneData[11];
          zoneConfig['balance'] = zoneData[12];
        }
      }
      i = i + 14;
    }
  	return validResponse ? zoneConfig : false;
  }
  
  debugOutput = (data) => {
  	var y_array = {1:'', 2:'', 3:'', 4:'', 5:'', 6:''};
  	var z_array = {1:'', 2:'', 3:'', 4:'', 5:'', 6:''};
  	for (let y = 0, len = this.zones.length; y < len; y++) {
			y_array[y+1] = this.zones[y].input_status;
		}
		for (let i = 0, len = this.zones.length; i < len; i++) {
			z_array[i+1] = this.zones[i].zone_status;
		}
		this.log.warn('-- Input Status:');
		this.log.warn(y_array);
		this.log.warn('-- Zone Status:');
		this.log.warn(z_array);
  	this.log.warn('-- Zone: ' + data['zone']);
  	this.log.warn('-- Power: ' + data['power']);
  	this.log.warn('-- Mute: ' + data['mute']);
  	this.log.warn('-- Mode: ' + data['mode']);
  	this.log.warn('-- Party: ' + data['party']);
  	this.log.warn('-- Input: ' + data['input']);
  	this.log.warn('-- Volume: ' + data['volume']);
  	this.log.warn('-- Treble: ' + data['treble']);
  	this.log.warn('-- Bass: ' + data['bass']);
  	this.log.warn('-- Balance: ' + data['balance'] + '\n');
  }
	
	setupGateway = (log) => {
    this.client.on('data', this.eventData.bind(this));
    this.client.connect(this.port, this.host, function() {
      log('Connected to HTD gateway...');   
  	});
  }
  
  executeCommand = (command) => {
    this.client.write(command,'binary');
  }

	configureAccessory(platformAccessory) {
		this.log.debug('configurePlatformAccessory');
	}

	removeAccessory(platformAccessory) {
		this.log.debug('removePlatformAccessory');
		this.api.unregisterPlatformAccessories(PLUGIN_NAME, PLATFORM_NAME, [platformAccessory]);
	}
}


class HtdZone {
	constructor(log, config, api, platform, zoneIndex) {
		this.log = log;
		this.api = api;
		this.config = config;
		this.platform = platform;
		this.zoneIndex = zoneIndex;
		this.zoneConfig = platform.config_zones_array[zoneIndex];
		this.zone_status = null;
		this.input_status = null;
		this.name = this.zoneConfig;
		this.inputServices = [];
		this.inputList = [];
		this.accessoryConfigured = false;
		this.manufacturer = this.zoneConfig.manufacturer || "HTD";
		this.modelName = this.zoneConfig.modelName || PLATFORM_NAME;
		this.serialNumber = this.zoneConfig.serialNumber || 'MCA-66';
		this.firmwareRevision = this.zoneConfig.firmwareRevision || PLUGIN_VERSION;
		this.prepareAccessory();
	}
	
	prepareAccessory() {
		if (this.accessoryConfigured) { return }
		const accessoryName = this.name;
		const uuid = UUID.generate(this.name); 
		let accessoryCategory = Categories.AUDIO_RECEIVER;
		this.accessory = new Accessory(accessoryName, uuid, accessoryCategory);
		this.prepareAccessoryInformationService();
		this.prepareTelevisionService();
		//this.prepareTelevisionSpeakerService();
		this.prepareInputSourceServices();
		this.api.publishExternalAccessories(PLUGIN_NAME, [this.accessory]);
		this.accessoryConfigured = true;
	}

	prepareAccessoryInformationService() {
		this.accessory.removeService(this.accessory.getService(Service.AccessoryInformation));
		const informationService = new Service.AccessoryInformation();
		informationService
			.setCharacteristic(Characteristic.Name, this.name)
			.setCharacteristic(Characteristic.Manufacturer, this.manufacturer)
			.setCharacteristic(Characteristic.Model, this.modelName)
			.setCharacteristic(Characteristic.SerialNumber, this.serialNumber)
			.setCharacteristic(Characteristic.FirmwareRevision, this.firmwareRevision)
		this.accessory.addService(informationService);
	}

	prepareTelevisionService() {
		this.televisionService = new Service.Television(this.name, 'televisionService');
		this.televisionService
			.setCharacteristic(Characteristic.ConfiguredName, this.name)
			.setCharacteristic(Characteristic.SleepDiscoveryMode, Characteristic.SleepDiscoveryMode.ALWAYS_DISCOVERABLE);
		this.televisionService.getCharacteristic(Characteristic.Active)
			.on('get', this.getPower.bind(this))
			.on('set', this.setPower.bind(this));
		this.televisionService.getCharacteristic(Characteristic.ActiveIdentifier)
			.on('get', this.getInput.bind(this))
			.on('set', (newInputIdentifier, callback) => { this.setInput(this.inputList[newInputIdentifier], callback); });
		this.accessory.addService(this.televisionService);
	}
	
	prepareTelevisionSpeakerService() {
		this.speakerService = new Service.TelevisionSpeaker(this.name + ' Speaker', 'speakerService');
		this.speakerService
			.setCharacteristic(Characteristic.Active, Characteristic.Active.ACTIVE)
			.setCharacteristic(Characteristic.VolumeControlType, Characteristic.VolumeControlType.RELATIVE);
		this.speakerService.getCharacteristic(Characteristic.VolumeSelector)
		  .on('set', (direction, callback) => { this.setVolume(direction, callback); });
		this.speakerService.getCharacteristic(Characteristic.Volume)
			.on('set', this.setVolume.bind(this));
		this.speakerService.getCharacteristic(Characteristic.Mute)
			.on('set', this.setMute.bind(this));
		this.accessory.addService(this.speakerService);
		this.televisionService.addLinkedService(this.speakerService);
	}
	
	prepareInputSourceServices() {
		this.inputList.push({inputId: '0', inputName: 'Dummy'});
		var config_inputs_array = Object.values(this.config.inputs[0]);
		if(config_inputs_array){
      for(let i = 0, len = config_inputs_array.length; i < len; i++) {
    		var configState = Characteristic.IsConfigured.NOT_CONFIGURED;
    		var visState = Characteristic.CurrentVisibilityState.HIDDEN;
    		if (config_inputs_array[i]) {
    			configState = Characteristic.IsConfigured.CONFIGURED;
    			visState = Characteristic.CurrentVisibilityState.SHOWN;
    		}
    		let inputService = new Service.InputSource(1, "input_" + (i+1).toString());
				inputService
					.setCharacteristic(Characteristic.Identifier, i+1)
					.setCharacteristic(Characteristic.ConfiguredName, config_inputs_array[i] || 'input_' + (i+1).toString())
					.setCharacteristic(Characteristic.InputSourceType, Characteristic.InputSourceType.AIRPLAY)
					.setCharacteristic(Characteristic.InputDeviceType, Characteristic.InputDeviceType.AUDIO_SYSTEM)
					.setCharacteristic(Characteristic.IsConfigured, configState)
					.setCharacteristic(Characteristic.CurrentVisibilityState, visState)
					.setCharacteristic(Characteristic.TargetVisibilityState, visState);
    				this.inputServices.push(inputService);
    				this.accessory.addService(inputService);
    				this.televisionService.addLinkedService(inputService);
    				this.inputList.push({inputId: inputService.getCharacteristic(Characteristic.Identifier), inputName: inputService.getCharacteristic(Characteristic.ConfiguredName)});
      }
    }
	}
	
	updateDeviceState(power_state, input_state, callback) {
    const previousPowerState = this.zone_status;
    const currentPowerState = power_state || Characteristic.Active.INACTIVE;
    const previousInputState = this.input_status;
    const currentInputState = input_state || NO_INPUT_ID;
		if (previousPowerState !== currentPowerState) {
			this.log('%s: Power changed from %s to %s', this.name, previousPowerState, currentPowerState);
			this.televisionService.getCharacteristic(Characteristic.Active).updateValue(currentPowerState);
			this.zone_status = currentPowerState;
		} else {
			if(!this.zone_status) {
  		  this.zone_status = currentPowerState;
			}
		}
		if (previousInputState !== currentInputState) {
			this.log('%s: Input changed from %s to %s', this.name, previousInputState, currentInputState);
			this.televisionService.getCharacteristic(Characteristic.ActiveIdentifier).updateValue(currentInputState);
			this.input_status = currentInputState;
		} else {
			if(!this.input_status) {
  		  this.input_status = currentInputState;
			}
		}
		return null;
	}

  getPower(callback) {
		if(this.zone_status != null) {
  		callback(null, this.zone_status ? Characteristic.Active.ACTIVE : Characteristic.Active.INACTIVE);
    } else {
  		this.platform.updated_flag = false;
  		var statusCommand = this.platform.getZoneStates((Number(this.zoneIndex)+1));
      var command = new Buffer(statusCommand, 'hex').toString('ascii');
    	this.platform.executeCommand(command);
    	setTimeout(() => {
    		if(this.platform.updated_flag == true) {
    			callback(null, this.zone_status ? Characteristic.Active.ACTIVE : Characteristic.Active.INACTIVE);
    		} else {
    			this.log.error('Couldn\'t get status for: ' + this.name);
    			callback(new Error("Error getting state."));
    		}
    	}, 1500);
    }
	}

	setPower(targetPowerState, callback) {
		if(this.zone_status != targetPowerState) {
			var powerCommand = this.platform.setPower((Number(this.zoneIndex)+1), targetPowerState == Characteristic.Active.INACTIVE ? 0 : 1);
      var command = new Buffer(powerCommand, 'hex').toString('ascii');
      this.platform.executeCommand(command);
		}
		callback(null); 
	}

	getInput(callback) {
		if(this.input_status != null) {
  		callback(null, this.input_status || NO_INPUT_ID);
    } else {
  		this.platform.updated_flag = false;
  		var statusCommand = this.platform.getZoneStates((Number(this.zoneIndex)+1));
      var command = new Buffer(statusCommand, 'hex').toString('ascii');
    	this.platform.executeCommand(command);
    	setTimeout(() => {
    		if(this.platform.updated_flag == true) {
    			callback(null, this.input_status || NO_INPUT_ID);
    		} else {
    			this.log.error('Couldn\'t get status for: ' + this.name);
    			callback(new Error("Error getting input."));
    		}
    	}, 1500);
    }
	}

	setInput(input, callback) {
		if(this.input_status != input) {
      var sourceCommand = this.platform.getSourceCommand((Number(this.zoneIndex)+1), input.inputId.value);
      var command = new Buffer(sourceCommand, 'hex').toString('ascii');
      this.platform.executeCommand(command);
		}
		callback(null);
	}
	
  setMute(muteState, callbackMute) {
		this.log.warn('%s: setMute: muteState:', this.name, muteState);

		if (callbackMute && typeof(callbackMute) === 'function') { 
			callbackMute();
		}
		this.log('%s: Set mute: %s', this.name, (muteState) ? 'Muted' : 'Not muted');
	}

	setVolume(volumeSelectorValue, callback) {
		this.log.warn('VOL');
		callback(null);
	}
};
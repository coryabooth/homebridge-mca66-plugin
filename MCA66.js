const net = require('net');
const client = new net.Socket();
const zones = [
  'AgABBgAJ',
  'AgACBgAK',
  'AgADBgAL',
  'AgAEBgAM',
  'AgAFBgAN',
  'AgAGBgAO'
];

let zoneIndex = 0;

client.connect(10006, '10.0.1.217', function() {
  sendNextZone();    
});

function sendNextZone() {
	if (zoneIndex < zones.length) {
	  client.write(Buffer.from(zones[zoneIndex++], 'base64').toString('ascii'));
  } else {
    client.destroy();
  }
}

client.on('data', function(data) {
	let data_array = new Uint8Array(data);
	let decoded = [];
  
	decoded['zone'] = data_array[16];
	decoded['power'] = data_array[18] == 128 ? 'On' : data_array[18] == 0 ? 'Off' : data_array[18] == 192 ? 'Muted' : 'Unknown';
	decoded['input'] = data_array[22] + 1;
	decoded['vol'] = data_array[23] - 196;
	decoded['mute'] = data_array[18] == 192 ? 'On' : 'Off';
	decoded['state'] = (data_array[18] == 128) || (data_array[18] == 192) ? true : false;
	
	console.log('\n');
	console.log('-- Zone: ' + decoded['zone']);
	console.log('-- Power: ' + decoded['power']);
	console.log('-- Input: ' + decoded['input']);
	console.log('-- Volume: ' + decoded['vol']);
	console.log('-- Mute: ' + decoded['mute']);
	console.log('\n');
	// done processing this zone, send the command for the next one
  sendNextZone();
});

client.on('close', function() {
});

client.on('error', function(err) {
	console.log(err);
});